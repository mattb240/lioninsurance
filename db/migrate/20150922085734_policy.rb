class Policy < ActiveRecord::Migration
  def up
     create_table :policies do |t|
     	t.string 'names'
     	t.string 'policy_number'
     	t.string 'from'
     	t.string 'to'
     	t.timestamps
     end 
  end

  def down
  	drop_table :policies
  end
end
