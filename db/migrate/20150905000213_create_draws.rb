class CreateDraws < ActiveRecord::Migration
  def up
    create_table :draws do |t|
      t.string "uuid"
      t.string "names"
      t.string "phone_number"
      t.string "item_insured"
      t.string "b1"
      t.string "ass"
      t.string "policy"
      t.string "policy_number"
      t.timestamps
    end
  end


  def down
  	drop_table :draws
  end
end
