class CreateWons < ActiveRecord::Migration
  
  def up
    create_table :wons do |t|
      t.string "uuid"
      t.string "phone_number"
      t.string "prize"
      t.timestamps
    end
  end


  def down
      drop_table :wons  
  end
end
