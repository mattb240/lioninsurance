class CreatePrizes < ActiveRecord::Migration
  def up
    create_table :prizes do |t|
      t.string "uuid"
      t.string "prize_name"
      t.timestamps
    end
  end

  def down
  	drop_table :prizes
  end
end
