class CreateSubmissions < ActiveRecord::Migration
  def up
    create_table :submissions do |t|
      t.string 'uuid'
      t.string 'phone_number'
      t.string 'item_1'
      t.string 'item_2'
      t.string 'item_3'
      t.timestamps
    end
  end

  def down
  	drop_table :submissions
  end
end
