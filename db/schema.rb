# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151012160104) do

  create_table "draws", force: true do |t|
    t.string   "uuid"
    t.string   "names"
    t.string   "phone_number"
    t.string   "item_insured"
    t.string   "b1"
    t.string   "ass"
    t.string   "policy"
    t.string   "policy_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "policies", force: true do |t|
    t.string   "names"
    t.string   "policy_number"
    t.string   "from"
    t.string   "to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "prizes", force: true do |t|
    t.string   "uuid"
    t.string   "prize_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "submissions", force: true do |t|
    t.string   "uuid"
    t.string   "phone_number"
    t.string   "item_1"
    t.string   "item_2"
    t.string   "item_3"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "password"
    t.string   "password_digest"
    t.string   "username"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "wons", force: true do |t|
    t.string   "uuid"
    t.string   "phone_number"
    t.string   "prize"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
