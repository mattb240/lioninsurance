class ImportController < ApplicationController
    
    def index
    	render template: 'import/index.html.erb'
    end

    def process_import
    	 Policy.import(params[:file])
         redirect_to :action => 'index'
    	 flash[:success] = 'Policies imported'
    end
end