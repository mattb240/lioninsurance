class IndexController < ApplicationController

	def index 
		render template: 'index/index'
	end
    
    def process_submit
    	if params[:phone_number].present? && params[:item_1].present? && params[:item_2].present? && params[:item_3].present?
    		@submission = Submission.new 
    		@submission.phone_number = params[:phone_number]
    		@submission.item_1 = params[:item_1]
    		@submission.item_2 = params[:item_2]
    		@submission.item_3 = params[:item_3]
    		if @submission.save
    			@prizes = Prizes.all 
    		    @prize_ids = Array.new
    		    @prizes.each do |id|
    		    	@prize_ids << id[:id]
    		    end
    	        @prize_id = @prize_ids.shuffle.sample
    	        @prize = Prizes.find(@prize_id)
    	        if @prize[:prize_name] == 'Bottle'
    	        	flash[:prize] = 'bottle'
    	        elsif @prize[:prize_name] == 'Wallet'
    	        	flash[:prize] = 'wallet'
                elsif @prize[:prize] == 'Umbrella'
                    flash[:prize] = 'umbrella'  
    	        elsif @prize[:prize_name] == 'KeyHolder'
    	        	flash[:prize] = 'key_holder'
    	        elsif @prize[:prize_name] == 'MBs'
                    ### Call the API to activate the mbs.....
    	        	flash[:prize] = 'MB'
                #################################
    	        elsif @prize[:prize_name] == 'GymBag'
    	        	flash[:prize] = 'gym_bag'
    	        elsif @prize[:prize_name] == 'NotePad'
                    flash[:prize] = 'note_pad'
                end
                @won = Won.new 
                @won.phone_number = params[:phone_number]
                @won.prize = @prize[:prize_name]
                if @won.save
    	           render template: 'index/prize'
                else
                   redirect_to :action => 'index'
                   flash[:error_notice] = 'Submission failed'
                end
    		else
    			redirect_to :action => 'index'
    			flash[:error_notice] = 'Submission failed'
    		end
    		
    	else
    		redirect_to :action => 'index'
    		flash[:error_notice] = 'Missing fields. Please try again'
    	end
    end
end
