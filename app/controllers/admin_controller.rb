class AdminController < ApplicationController

	layout :false 

	def index
		render template: 'admin/signin'
	end

	def process_sign_in
		if params[:username].present? && params[:password].present? 
			if @user = User.find_by(username: params[:username])
				if @user.authenticate(params[:password])
				    session[:user_id] = @user['id']
                    session[:first_name] = @user['first_name']
                    session[:last_name] = @user['last_name']
                    session[:username] = @user['username']
				    redirect_to :action => 'home'
			    else
				    flash[:error_notice] = 'Wrong email address or password'
			        redirect_to :action => 'index'
			    end
			else
			    flash[:error_notice] = 'Wrong email address or password'
			    redirect_to :action => 'index'
			end	
		else
			flash[:error_notice] = 'Wrong email address or password'
			redirect_to :action => 'index'
		end
	end

	def process_sign_up
		if session[:user_id].nil?
			flash[:error_notice] = 'Please login with admin rights'
			redirect_to :action => 'users'
		else
		    if params[:username].present? && params[:password].present? && params[:first_name].present? && params[:last_name].present?
			@user = User.new
			@user.assign_attributes(first_name: params[:first_name], 
				                     last_name: params[:last_name], 
				                     password: params[:password], 
				                     username: params[:username]
				                    )
			if @user.save
				flash[:success] = 'New user has been created'
				redirect_to :controller => 'admin', :action => 'users'
			else
				flash[:error_notice] = @user.errors.full_messages
			    redirect_to :controller => 'admin', :action => 'users'
			end
		    else
		    	flash[:error_notice] = 'Something went wrong. User was not created, Missing fields'
		    	redirect_to :controller => 'admin',:action => 'users'
		    end
		end
	end

	def home
		if session[:user_id].nil?
			flash[:error_notice] = 'Please login with admin rights'
			redirect_to :action => 'index'
		else
			@draws = Draw.all.sorted
			render template: 'admin/draws'
		end
	end

	def users
		if session[:user_id].nil?
			flash[:error_notice] = 'Please login with admin rights'
			redirect_to :action => 'index'
		else
			@users = User.all.sorted
			render template: 'admin/users'
		end
	end


	def prizes 
		if session[:user_id].nil?
			flash[:error_notice] = 'Please login with admin rights'
			redirect_to :action => 'index'
		else
			@won = Won.all.sorted
			render template: 'admin/winners'
		end
	end

	def signout
		session[:user_id] = nil
        session[:first_name] = nil
        session[:last_name] = nil
        session[:username] = nil
        flash[:success] = 'Successfully Signed out'
        redirect_to :controller => 'admin', :action => 'index'
	end

end