class HomeController < ApplicationController

	def index
		render template: 'home/index'
	end

	def thankyou
		if params[:phone_number].present? && params[:names].present? && params[:f1].present? && params[:f2].present? && params[:item_insured].present? && params[:f3].present? && params[:policy_number].present? 
		    @draw = Draw.new
		    @draw.phone_number = params[:phone_number]
		    @draw.names = params[:names]
		    @draw.b1 = params[:f1]
		    @draw.ass = params[:f2]
		    @draw.item_insured = params[:item_insured]
		    @draw.policy  = params[:f3]
		    @draw.policy_number = params[:policy_number]
		    policy_number = params[:f1]+"/"+params[:f2]+"/"+params[:f3]+"/"+params[:policy_number]
		    if @policy = Policy.find_by(:policy_number => policy_number)
		    	puts "Policy is found"
		    	if @draw.save
		    	   render template: 'home/thankyou' 	
    		    else
    		    	redirect_to :action => 'index'
    		        flash[:error_notice] = 'Something went wrong.. Please try again later'
    		    end
		    else
		    	redirect_to :action => 'index'
    		    flash[:error_notice] = 'Policy number is not allowed'
		    end
		    
		else
			redirect_to :action => 'index'
    		flash[:error_notice] = 'Missing fields. Please try again'
		end
	end


	def terms 
		render template: 'home/terms'
	end


	# def thankyou 
	# 	render template: 'home/thankyou'
	# end
end
