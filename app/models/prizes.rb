class Prizes < ActiveRecord::Base
	before_create :generate_uuid

   private 
   def generate_uuid
      self.uuid = SecureRandom.uuid.to_s+"-"+Time.now.to_i.to_s
   end
end
