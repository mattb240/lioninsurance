class User < ActiveRecord::Base

	has_secure_password

	validates :first_name,
	          :presence => true

	validates :last_name, 
	          :presence => true

	validates :username, 
	          :presence => true,
	          :uniqueness => true

	validates :password,
	          :presence => true

	          scope :sorted, lambda {order("created_at DESC")}
end
