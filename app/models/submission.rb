class Submission < ActiveRecord::Base

	before_create :generate_uuid

	validates :phone_number, 
	          :presence => true

	validates :item_1, 
	          :presence => true 

	validates :item_2,
	          :presence => true

	validates :item_3, 
	          :presence => true


    def generate_uuid 
    	self.uuid = SecureRandom.uuid.to_s+'-'+Time.now.to_i.to_s
    end

end
