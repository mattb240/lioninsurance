Rails.application.routes.draw do
  root 'index#index'
  match 'import' => 'import#index', :via => :get
  match 'home/terms' => 'home#terms', :via => :get
  match 'process_import' => 'import#process_import', :via => :post
  match 'submit' => 'index#process_submit', :via => :post 
  match 'home/thankyou' => 'home#thankyou', :via => :post 
  match ':controller(/:action/:id)', :via => :get
  match 'admin' => 'admin#index', :via => :get
  match 'admin/process_sign_in' => 'admin#process_sign_in', :via => :post
  match 'admin/process_sign_up' => 'admin#process_sign_up', :via => :post
  match 'admin/home' => 'admin#home', :via => :get
  match 'admin/users' => 'admin#users', :via => :get
  match 'admin/signout' => 'admin#signout', :via => :get
   match 'admin/winners' => 'admin#prizes', :via => :get
end
